<?php

namespace MidnightLuke\WikiReader\Twig\MarkdownEngine;

use Aptoma\Twig\Extension\MarkdownEngineInterface;
use Michelf\MarkdownExtra;
use MidnightLuke\WikiReader\Twig\Url;

class WikiMarkdownEngine implements MarkdownEngineInterface
{
    private $parser;

    public function __construct($context)
    {
        $this->parser = new MarkdownExtra();

        // push on custom URL handler
        $this->parser->url_filter_func = function ($url) use ($context) {
            return Url::url($context, $url);
        };

        // push on id generation handler
        $this->parser->header_id_func = function ($header) {
            return preg_replace('/[^a-z0-9]/', '-', strtolower($header));
        };
    }

    /**
     * {@inheritdoc}
     */
    public function transform($content)
    {
        return $this->parser->transform($content);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'Michelf\Markdown';
    }
}
