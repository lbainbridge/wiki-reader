<?php

namespace MidnightLuke\WikiReader\Twig;

class Url
{
    /**
     * Helper function to properly prefix absolute URLs and append query strings,
     * preserves specific queries for use in application.
     */
    public static function url($context, $path = null)
    {
        $request = $context['request'];

        // if this is passed as null use current path
        if (is_null($path)) {
            $path = $request->getPathInfo();
        }

        // prefix absolute URLs with basepath
        if (substr($path, 0, 1) == '/') {
            $path = $request->getBasePath() . $path;
        }

        return $path;
    }
}
