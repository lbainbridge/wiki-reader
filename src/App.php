<?php

namespace MidnightLuke\WikiReader;

use Aptoma\Twig\Extension\MarkdownExtension;
use MidnightLuke\WikiReader\Twig\Url;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation as Http;
use Symfony\Component\Yaml\Yaml;
use MidnightLuke\WikiReader\Twig\MarkdownEngine\WikiMarkdownEngine;

/**
 * Main application for BootFrame, can be used as follows:
 *
 * define('BASE_DIR', __DIR__ . '/..');
 * define('WEB_DIR', __DIR__);
 *
 * use BootFrame\App;
 *
 * $app = new App;
 * $app->run();
 */
class App
{
    protected $request;
    protected $config;
    protected $routes;

    public function __construct()
    {
        // parse the config
        $this->config = Yaml::parse(file_get_contents(BASE_DIR . '/config/config.yml'));
        foreach ($this->config['imports'] as $import) {
            $this->config += Yaml::parse(file_get_contents(BASE_DIR . '/config/' . $import['resource']));
        }

        if ($this->config['parameters']['debug']) {
            $whoops = new \Whoops\Run();
            $whoops->pushHandler(new \Whoops\Handler\PrettyPageHandler());
            $whoops->register();
        }
    }

    /**
     * Main entry point for the BootFrames app, will construct request, routes and
     * twig environment then run.
     */
    public function run()
    {
        // parse request
        $this->request = Http\Request::createFromGlobals();

        // do authorization
        $this->doAuth();

        // generate routing
        if ($this->config['routing_type'] == 'scan') {
            $wiki_path = $this->config['parameters']['wiki_path'];
            if (substr($wiki_path, 0, 1) != '/') {
                $wiki_path  = BASE_DIR . '/' . $wiki_path;
            }
            $this->routes = $this->wikiScan($wiki_path);
        } elseif ($this->config['routing_type'] == 'yaml') {
            $this->routes = Yaml::parse(file_get_contents(BASE_DIR . '/config/routes.yml'));
        }

        // setup twig environment
        $loader = new \Twig_Loader_Filesystem(BASE_DIR . '/templates');
        $twig = new \Twig_Environment($loader, $this->config['twig']);

        // path to match for routing
        $path = urldecode($this->request->getPathInfo());
        $route = $this->matchRoute($path, $this->routes);

        if ($route === null) {
            // 404
            $response = new Http\Response('404 - Not found', Http\Response::HTTP_NOT_FOUND);
            $response->send();
            exit;
        }

        // generate menu tree and trail
        $tree = $this->getTree();
        $trail = $this->getTrail($tree[$route['path']], $tree);

        // add some useful globals
        $twig->addGlobal('all_routes', $this->routes);
        $twig->addGlobal('request', $this->request);
        $twig->addGlobal('current_route', $route);
        $twig->addGlobal('trail', $trail);
        $twig->addGlobal('tree', $tree);
        $twig->addGlobal('wiki_title', $this->config['parameters']['title']);

        // custom url handler
        $twig->addFunction(new \Twig_SimpleFunction('url', [
            'MidnightLuke\\WikiReader\\Twig\\Url',
            'url'
        ], ['needs_context' => true]));
        if (class_exists('Kint')) {
            $twig->addFunction(new \Twig_SimpleFunction('dump', [
                'Kint',
                'dump'
            ]));
        }

        // add markdown engine.
        $mdengine = new WikiMarkdownEngine($twig->getGlobals());
        $twig->addExtension(new MarkdownExtension($mdengine));

        // render a twig template
        $response = new Http\Response(
            $twig->render(
                'template.html.twig',
                [
                    'routes' => $this->routes,
                    'document' => file_get_contents($wiki_path . '/' . $route['document']),
                ]
            )
        );

        // send the response
        $response->send();
    }

    /**
     * Scans directory structure for wireframes using configuration from
     * config.yml.  Uses the `path` component to fill in various missing details
     * for display purposes.
     *
     * @return A structured array of routes this application can understand.
     */
    private function wikiScan($dir)
    {
        // store routes here
        $routes = [];

        // get a finder
        $files = new Finder();
        $files->files()
            ->in($dir);

        // create routes from files
        foreach ($files as $file) {
            $path = $this->cleanPath($file->getRelativePathname());

            $routes[$path] = [
                'path' => $path,
                'document' => $file->getRelativePathname(),
                'title' => $this->getTitle($file->getRealPath()),
                'parent' => $this->getParent($path),
            ];
        }

        return $routes;
    }

    /**
     * Matches a route based on the path passed in.
     *
     * @param  string $path   The path to search for in the passed routes array.
     * @param  array  $routes A structured array of routes to search through.
     * @return array          The route within route array.
     */
    private function matchRoute($path, array $routes)
    {
        foreach ($routes as $route) {
            if ($path == $route['path']) {
                return $route;
            }
        }
        return null;
    }

    private function getTitle($path)
    {
        $file = fopen($path, 'r');
        while (($buffer = fgets($file, 4096)) !== false) {
            // Header.
            $start = substr($buffer, 0, 2);
            if ($start == "# ") {
                fclose($file);
                return trim($buffer, "# \n\r\t");
            }

            // Last line was header.
            if (isset($last)
                && !empty(trim($buffer))
                && strlen(str_replace('=', '', trim($buffer))) === 0) {
                fclose($file);
                return trim($last);
            }
            $last = $buffer;
        }
        fclose($file);
        return $path;
    }

    public function getTrail($leaf, $tree, &$return = [])
    {
        $return[] = $leaf['path'];
        if ($leaf['parent'] !== null) {
            return $this->getTrail($tree[$leaf['parent']], $tree, $return);
        }
        return $return;
    }

    /**
     * Function that performs simple basic auth.  If the user is not found in the
     * configured array this returns a 401 response and terminates the script.
     */
    private function doAuth()
    {
        // only BASIC auth is configured
        if ($this->config['auth']['type'] != 'basic') {
            return;
        }

        // get users from configuration
        $users = $this->config['auth']['users'];

        // start with null
        $username = $password = '';

        // mod_php
        if (isset($_SERVER['PHP_AUTH_USER'])) {
            $username = $_SERVER['PHP_AUTH_USER'];
            $password = $_SERVER['PHP_AUTH_PW'];

            // most other servers
        } elseif (isset($_SERVER['HTTP_AUTHORIZATION'])) {
            if (strpos(strtolower($_SERVER['HTTP_AUTHORIZATION']), 'basic')===0) {
                list($username,$password) = explode(':', base64_decode(substr($_SERVER['HTTP_AUTHORIZATION'], 6)));
            }
        } elseif (isset($_SERVER['Authorization'])) {
            if (strpos(strtolower($_SERVER['Authorization']), 'basic')===0) {
                list($username,$password) = explode(':', base64_decode(substr($_SERVER['Authorization'], 6)));
            }
        }

        // continue to send 401 until user is authorized correctly
        if (!isset($users[$username]) || $users[$username] != $password) {
            $response = new Http\Response(
                '401 - Not authorized',
                Http\Response::HTTP_UNAUTHORIZED,
                [
                    'WWW-Authenticate' => 'Basic realm="Boot Frames"',
                ]
            );
            $response->send();
            exit();
        }
    }

    public function getTree()
    {
        $routes = $this->routes;
        $tree = [];
        $finder = new Finder();
        $wiki_path = $this->config['parameters']['wiki_path'];
        if (substr($wiki_path, 0, 1) != '/') {
            $wiki_path  = BASE_DIR . '/' . $wiki_path;
        }
        $finder->in($wiki_path);
        foreach ($finder as $resource) {
            $path = $this->cleanPath($resource->getRelativePathname());
            if (!isset($tree[$path])) {
                $tree[$path] = array(
                    'title' => (isset($routes[$path]) ? $routes[$path]['title'] : $resource->getFilename()),
                    'linked' => (bool) (isset($routes[$path])),
                    'path' => $path,
                    'parent' => $this->getParent($path),
                    'children' => false,
                );
            }
            if (isset($tree[$path]['parent'])) {
                $tree[$tree[$path]['parent']]['children'] = true;
            }
        }

        return $tree;
    }

    private function cleanPath($path)
    {
        $path = '/' . str_replace(['.md', '.mdown'], '', $path);

        // hide the "index" portion of the path
        if (substr($path, -5) == 'index') {
            $path = substr($path, 0, -5);

            // cut off extra character
            if (strlen($path) > 1) {
                $path = substr($path, 0, -1);
            }
        }

        return $path;
    }

    private function getParent($path)
    {
        // pop off the last section of the path to determine parent
        $parent = substr($path, 0, strrpos($path, '/'));

        return (empty($parent) ? null : $parent);
    }
}
