<?php

require __DIR__ . '/../vendor/autoload.php';

define('BASE_DIR', __DIR__ . '/..');
define('WEB_DIR', __DIR__);

$app = new MidnightLuke\WikiReader\App();
$app->run();
